class SliderAction {
  constructor(divSlides) {
    this.slideIndex = 1;
    this.slides = divSlides;
  }

  showDivs(n) {
    var i;

    if (n > this.slides.length) {
      this.slideIndex = 1;
    }
    if (n < 1) {
      this.slideIndex = this.slides.length;
    }
    for (i = 0; i < this.slides.length; i++) {
      this.slides[i].style.display = "none";
    }
    this.slides[this.slideIndex - 1].style.display = "block";
  }

  plusDivs(n) {
    this.showDivs((this.slideIndex += n));
  }
}

export { SliderAction };
