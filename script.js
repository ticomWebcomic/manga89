import { SliderAction } from "/sliderAction.js";
import { AudioAction } from "/audioAction.js";

var botonNext = document.getElementById("botonNext");
var botonPrev = document.getElementById("botonBack");
var slider = document.getElementsByClassName("mySlides");
var audio = document.getElementById("audio");

var sliderAction = new SliderAction(slider);
var audioAction = new AudioAction(audio);

botonNext.addEventListener("click", function() {
  sliderAction.plusDivs(1);
  audioAction.play();
});

botonPrev.addEventListener("click", function() {
  sliderAction.plusDivs(-1);
  audioAction.play();
});

sliderAction.showDivs(1);
