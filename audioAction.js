class AudioAction {
  constructor(audioElement) {
    this.audio = audioElement;
    this.comenzo = 0;
  }

  play() {
    console.log(this.audio.paused);

    if (this.comenzo === 0) {
      this.audio.currentTime = 10;
      this.comenzo = 1;
    }

    if (this.audio.paused === true) {
      this.audio.play();
    }
  }
}

export { AudioAction };
