Manga 89
=================

Un ejemplo de crear un comic multimedia con musica e imagenes que cambian en HTML con JS

Your Project
------------

### ← README.md

Este archivo, donde se describe el contenido del proyecto

### ← index.html

El comic en formato de presentación

### ← style.css

El estilo de la pagina

### ← script.js

El script que carga los eventos en las diferentes partes de la interfaz

### ← audioAction.js

El script con las funciones para manejar el audio

### ← sliderAction.js

El script con las funciones para manejar las diapositivas

### ← assets

Drag in `assets`, like images or music, to add them to your project

Made by [Glitch](https://glitch.com/)
-------------------

\ ゜o゜)ノ
